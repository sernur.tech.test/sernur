'use strict';

var frontpage = document.querySelector('[data-frontpage]');

frontpage && carouselSlider();

function carouselSlider() {

  var slider = document.querySelectorAll('.slider');
  slider.forEach(function (e, i) {
    var slider_list = e.querySelector('.slider__ul');
    var slide_left = e.querySelector('.slider__btn--prev');
    var slide_right = e.querySelector('.slider__btn--next');
    var count = slider_list.querySelectorAll('li').length;
    var clicks = 0;
    slide_right.addEventListener('click', moveLeft);

    function moveLeft() {
      clicks++;
      var x = 370;
      slide_left.style.display = 'block';
      slider_list.style.transform = 'translateX(' + (0 - x * clicks) + 'px)';
      if (count - clicks <= 3) {
        slide_right.style.display = 'none';
      }
    }

    slide_left.addEventListener('click', moveRight);

    function moveRight() {
      clicks--;
      var x = 370;
      slide_right.style.display = 'block';
      if (clicks == 0) {
        var y = 'translateX(' + 0 + 'px)';
      } else {
        var y = 'translateX(' + x * clicks * -1 + 'px)';
      }
      if (clicks == 3) {
        clicks = 0;
      }
      slider_list.style.transform = y;

      if (slider_list.style.transform == 'translateX(0px)') {
        slide_left.style.display = 'none';
      }
    }
  });

  $(document).ready(function () {

    $(".reviews__item").click(function () {
      event.preventDefault();
      $(".reviews__blockquote").hide();
      var BlockTrigger = $(this).data("review");
      $("#blockquote-" + BlockTrigger).show();
      $('.reviews__item').removeClass('reviews__item--active'); 
      $(this).addClass('reviews__item--active');

      if (BlockTrigger == 7) {
        $('.btn-block').hide();
      } else {
        $('.btn-block').show();
      }
    });
  });
};

frontpage && $(window).bind('scroll', function (e) {
  parallaxScroll();
});

function parallaxScroll() {
  var scrolled = $(window).scrollTop();
  $('.milk-big').css({
    'top': 480 + scrolled * .25 + 'px',
    'right': -131 + scrolled * .15 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.milk-small').css({
    'top': 200 - scrolled * .5 + 'px',
    'left': 131 + scrolled * .25 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.butter').css({
    'top': 94 + scrolled * .55 + 'px',
    'left': 61 - scrolled * .25 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.bottle').css({
    'top': 530 + scrolled * .3 + 'px',
    'left': -110 + scrolled * .25 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.cheese-big').css({
    'top': 87 - scrolled * .5 + 'px',
    'right': 0 + scrolled * .05 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.cheese-small').css({
    'top': 679 - scrolled * .15 + 'px',
    'left': 487 - scrolled * .05 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.list').css({
    'top': -22 + scrolled * .55 + 'px',
    'left': 807 - scrolled * .55 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
}
