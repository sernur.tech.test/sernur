class Counter {
	constructor() {
		this.selectors = {
			wrap: '.js-counter',
			native: '.js-counter-native',
			field: '.js-counter-field',
			minus: '.js-counter-minus',
			plus: '.js-counter-plus'
		};

		this.classes = {
			disabled: 'counter__btn_disabled'
		};

		this.data = {
			increment: 'increment',
			postfix: 'postfix',
			max: 'max',
			min: 'min',
			ratio: 'ratio',
			negative: 'negative',
			eventChange: 'event-change'
		}

		this.el = $(this.selectors.wrap);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({native, field, minus, plus}, data) {
		const {increment, initial, postfix, max, min, ratio, negative, eventChange} = data;

		this.elems = this.el.toArray().map((el) => {

			const	$el = $(el),
					minValue = $el.data(negative) ? Number.NEGATIVE_INFINITY : 0;

			return {
				$el,
				$native: $el.find(native),
				$field: $el.find(field),
				$minus: $el.find(minus),
				$plus: $el.find(plus),
				data: {
					increment: this.toFloat($el.data(increment)) || 1,
					value: this.toFloat($el.find(native).val()),
					postfix: $el.data(postfix),
					max: this.toInt($el.data(max)) || Number.POSITIVE_INFINITY,
					min: this.toFloat($el.data(min)) || minValue,
					ratio: this.toFloat($el.data(ratio)) || 1,
					eventChange: $el.data(eventChange) || 'counter:change'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(elem => {
			elem.$minus.on('click', {that: this, elem, add: false}, this.change);
			elem.$plus.on('click', {that: this, elem, add: true}, this.change);
			elem.$el.on('counter:get', {that: this, elem}, (e) => e.data.that.setValue(e.data.that, e.data.elem));
		});
	}

	change(e) {
		const	{elem, that, add} = e.data,
				{increment, negative, max, min} = elem.data,
				{disabled} = that.classes;

		let {mayIncrease, mayDecrease} = that.getPossibility(elem.data);

		if(add && mayIncrease) {
			elem.data.value = that.add(elem.data.value, increment);
		}

		if(!add && mayDecrease) {
			elem.data.value = that.sub(elem.data.value, increment);
		}

		({mayIncrease, mayDecrease} = that.getPossibility(elem.data));
		elem.$plus.toggleClass(disabled, !mayIncrease);
		elem.$minus.toggleClass(disabled, !mayDecrease);

		that.setValue(that, elem);
	}

	getPossibility({increment, value, max, min}) {
		const 	rate = 1000,
				fixedVal = value * rate,
				fixedInc = increment * rate;

		return {
			mayIncrease: fixedVal + fixedInc <= max * rate,
			mayDecrease: fixedVal - fixedInc >= min * rate
		}
	}

	setValue(that, elem) {
		const {postfix, ratio, value, eventChange} = elem.data;
		let result, resultPostfix, resultComma;

		result = value * ratio;

		resultComma = result.toString().replace('.', ',');

		resultPostfix = resultComma + (postfix ? ` ${postfix}` : '');

		elem.$native.val(value);
		elem.$field.text(resultPostfix);

		elem.$el.trigger(eventChange, {value, result});
	}

	add(a, b) {
		const rate = 1000;
		let res = a * rate + b * rate;
		return res / rate;
	}

	sub(a, b) {
		const rate = 1000;
		let res = a * rate - b * rate;
		return res / rate;
	}

	toInt(data) {
		if(typeof data !== 'undefined' && data !== null) {
			return parseInt(data);
		}
		else {
			return undefined;
		}
	}

	toFloat(data) {
		if(typeof data !== 'undefined' && data !== null) {
			return parseFloat(data);
		}
		else {
			return undefined;
		}
	}
}
