class Purchase {
	constructor() {
		this.selectors = {
			el: '.js-purchase',
			repeat: '.js-purchase-repeat',
			form: '.js-purchase-form',
			more: '.js-purchase-more'
		};

		this.classes = {
			more: 'purchase_more'
		};

		this.data = {
		}

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({repeat, form, more}, data) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$repeat: $el.find(repeat),
				$form: $el.find(form),
				$more: $el.find(more),
				data: {
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$repeat.on('click', {that: this, el}, this.sendForm);
			el.$more.on('click', {that: this, el}, this.more);
		});
	}

	sendForm(e) {
		const {that, el} = e.data;

		el.$form.submit();
	}

	more(e) {
		const {that, el} = e.data;

		el.$el.addClass(that.classes.more);
	}
}
