class Bill {
	constructor() {
		this.selectors = {
			el: '.js-bill',
			shade: '.js-bill-shade',
			btn: '.js-bill-btn',
			form: '.js-bill-form',
			close: '.js-bill-close',
			text: '.js-bill-text',
			link: '.js-bill-link',
			amount: '.js-bill-amount'
		};

		this.classes = {
			show: 'bill_show',
			answer: 'bill_answer',
			fixed: 'bill_fixed'
		};

		this.data = {
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({shade, btn, form, close, text, link, amount}, {}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$shade: $el.find(shade),
				$btn: $el.find(btn),
				$form: $el.find(form),
				$close: $el.find(close),
				$text: $el.find(text),
				$link: $el.find(link),
				$amount: $el.find(amount),
				data: {
				}
			};
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			this.$window
				.on('bill:show', {that: this, el}, this.toggleForm)
				.on('bill:amount', {that: this, el}, this.setAmount);
			el.$shade.on('click', {that: this, el}, this.toggleForm);
			el.$close.on('click', {that: this, el}, this.toggleForm);
			el.$btn.on('click', {that: this, el}, this.validateForm);
		});
	}

	scroll(e) {
		const 	{that, el} = e.data,
				{fixed} = that.classes;

			el.$el.toggleClass(fixed, window.scrollY > 40);
	}

	setAmount(e, data) {
		const 	{that, el} = e.data;

		el.$amount.val(data.value);
	}

	toggleForm(e) {
		const 	{that, el} = e.data,
				{show} = that.classes;

		if(el.$el.hasClass(show)){
			that.$window.off('scroll', {that, el}, that.scroll);
		}
		else {
			that.$window.on('scroll', {that, el}, that.scroll);
			that.scroll({data: {el, that}});
		}

		el.$el.toggleClass(show);
	}

	validateForm(e) {
		e.preventDefault();

		const	{that, el} = e.data;
		let valid = true;

		el.$form.find('.js-field').each((i, item) =>{
			const $el = $(item);
			$el.trigger('field:validate');
			if($el.data('error') === true){
				valid = false;
			}
		});

		if(valid){
			that.sendForm(el);
		}
		else {
			console.log('Form is not valid.');
		}
	}

	sendForm(el) {
		const 	{$form} = el,
				url = $form.prop('action'),
				data = $form.serialize(),
				callback = this.result,
				context = this;
		let 	additional = $.param(el.$form.data());

		this.$window.trigger('ajax:get', {url, data: data + '&' + additional, callback, context: {that: this, el}});
	}

	result({data, context}) {
		const 	{that, el} = context;

		if(data.success) {
			el.$text.text(data.text);
			el.$link.html(data.link);
			el.$el.addClass(that.classes.answer);
		}
		else {
			console.log('error');
		}

	}
}
