class TextPage {
	constructor() {
		this.el = $('.text-page');

		if (this.el.length) {
			this.init();
		}
	}

	init() {
		const headers = Array.from(document.querySelectorAll('.header_h2'));
		const anchors = Array.from(document.querySelectorAll('.sidebar__part'));

		for (let index = 0; index < headers.length; index++) {
			const parsedIndex = headers[index].getAttribute('id');
			anchors[index].setAttribute('onClick', `textPage.jumpTo('#${parsedIndex}')`);
		}
	}

	jumpTo(header) {
		const offset = $(header).offset().top - 110;
		console.log(offset);
		$('html, body').animate({scrollTop: offset}, 200, 'swing');
	}
}
