class Comment {
	constructor() {
		this.selectors = {
			el: '.js-comment',
			add: '.js-comment-add',
			wrap: '.js-comment-wrap',
			field: '.js-field'
		};

		this.classes = {
			active: 'comment_active'
		};

		this.data = {};
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors)
				.init();
		}
	}

	get({add, wrap, field}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$add: $el.find(add),
				$wrap: $el.find(wrap),
				$field: $el.find(field)
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$add.on('click', {that: this, el}, this.toggleWrap);
			el.$field.on('field:change', this.change);
		});
	}

	toggleWrap(e) {
		e.preventDefault();

		const	{that, el} = e.data,
				{active} = that.classes;

		el.$el.toggleClass(active);
		el.$field.trigger('field:empty');
	}

	change(e) {
		console.log('change');
	}
}
