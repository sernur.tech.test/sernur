class ProductPage {
	constructor() {
		this.selectors = {
			el: '.js-product-page',
			sticky: '.js-product-sticky',
			bill: '.js-product-page-bill',
			quantity: '.js-product-page-quantity',
			price: '.js-product-page-price',
			btn: '.js-product-page-btn',
			form: '.js-product-page-form'
		};

		this.data = {
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({sticky, bill, quantity, price, btn, form}, {}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$sticky: $el.find(sticky),
				$bill: $el.find(bill),
				$quantity: $el.find(quantity),
				$btn: $el.find(btn),
				$form: $el.find(form),
				data: {
					price: $el.find(price).data('price')
				}
			};
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			Stickyfill.add(el.$sticky);

			el.$bill.on('click', {that: this}, this.showBill);
			el.$quantity.on('counter:change', {that: this, el}, this.setQuantity);
			el.$btn.on('click', {that: this, el}, this.addToCart);
		});
	}

	addToCart(e) {
		e.preventDefault();

		const 	{el, that} = e.data,
				{$form} = el,
				url = $form.prop('action'),
				data = $form.serialize(),
				callback = that.cartUpdate,
				additional = $.param(el.$form.data());

		el.$btn.prop('disabled', true);

		that.$window.trigger('ajax:get', {url, data: data + '&' + additional, callback, context: {that, el}});
	}

	cartUpdate({data, context}) {
		const	{that, el} = context;

		el.$btn.prop('disabled', false);

		that.$window.trigger('cart:update', data);

	}

	setQuantity(e, data) {
		const 	{el, that} = e.data,
				{result: value} = data;

		let summ = (Number.parseFloat((el.data.price * value)
			.toFixed(2).toString()))
			.toString().replace('.',',');

		el.$btn.text(`Купить за ${summ} ₽`);

		that.$window.trigger('bill:amount', {value});
	}

	showBill(e) {
		const 	{that} = e.data;

		that.$window.trigger('bill:show');
	}
}
