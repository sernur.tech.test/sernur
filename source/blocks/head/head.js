class Head {
	constructor() {
		this.selectors = {
			head: '.js-head',
			menuBtn: '.js-head-menu-btn',
			shade: '.js-head-shade',
			search: '.js-mobile-search',
			searchForm: '.js-head-search-form',
			searchList: '.js-head-search-list',
			searchAdd: '.js-head-search-add',
			searchBtn: '.js-head-search-btn',
			login: '.js-head-login',
			loginLink: '.js-head-login-link',
			loginTurn: '.js-head-login-turn',
			loginBtn: '.js-head-login-btn',
			loginMsg: '.js-head-login-msg',
			cart: {
				cart: '.js-cart',
				btn: '.js-cart-btn',
				amount: '.js-cart-amount',
				list: '.js-cart-list',
				order: '.js-cart-order'
			}
		};

		this.classes = {
			hideTop: 'head_top-hide',
			mobileMenu: 'head_mobile-menu',
			expandSearch: 'head_expand-search',
			cartListOpen: 'head_cart-list-open',
			search: 'head_search',
			login: 'head_login',
			recovery: 'head__login_recovery',
			amountHide: 'head__cart-amount_hide'
		};

		this.data = {
			cartAddUrl: 'cart-add-url',
			eventAdd: 'event-add'
		};

		this.desktopWidth = 1000;

		this.$window = $(window);
		this.el = $(this.selectors.head);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({menuBtn, shade, search, searchForm, cart, searchList, login, loginTurn, loginLink, loginBtn, loginMsg}, {cartAddUrl, eventAdd}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$menuBtn: $el.find(menuBtn),
				$shade: $el.find(shade),
				$search: $el.find(search),
				$searchForm: $el.find(searchForm),
				$searchList: $el.find(searchList),
				$searchField: $el.find(search).parent().find('.js-field'),
				login: {
					$login: $el.find(login),
					$loginLink: $el.find(loginLink),
					$loginTurn: $el.find(loginTurn),
					$loginBtns: $el.find(loginBtn),
					$loginFields: $el.find(login + ' .js-field'),
					$loginMsg: $el.find(loginMsg)
				},
				cart: {
					$cart: $el.find(cart.cart),
					$btn: $el.find(cart.btn),
					$amount: $el.find(cart.amount),
					$list: $el.find(cart.list),
					$order: $el.find(cart.order)
				},
				data: {
					cartAddUrl: $el.data(cartAddUrl),
					eventAdd: $el.data(eventAdd) || 'cart:add',
					showList: true,
					searchItemCount: $el.find(searchForm).data('count')
				}
			}
		});
		return this;
	}

	init() {
		this.checkMobile();
		this.$window.on('resize', {that: this}, this.checkMobile);

		this.elems.forEach(elem => {
			Stickyfill.add(elem.$el);

			elem.$menuBtn.on('click', {that: this, elem}, this.mobileMenu);
			elem.$shade.on('click', {that: this, elem}, this.hideShade);
			elem.$search.on('click', {that: this, elem}, this.expandSearch);
			elem.$searchField
				.on('focusin', {that: this, elem}, this.expandSearch)
				.on('field:input', {that: this, elem}, this.searchSet);

			elem.login.$loginLink.on('click', {that: this, elem}, this.toggleLogin);
			elem.login.$loginTurn.on('click', {that: this, elem}, this.loginTurn);
			elem.login.$loginFields.on('field:input', {that: this, elem}, this.validateLoginForm);
			elem.login.$loginBtns.on('click', {that: this, elem}, this.sendLoginForm);

			// elem.cart.$btn.on('click', {that:this, elem}, this.toggleCartList);
			// elem.cart.$amount.on('click', {that:this, elem}, this.toggleCartList);

			this.$window
				// .on('scroll', {that: this, elem}, this.hideTop)
				.on('cart:update cart:add', {that: this, elem}, this.renderCart)
				.on('cart:dontShowList', {elem}, (e) => e.data.elem.data.showList = false);
		});
	}

	loginTurn(e) {
		const	{that, elem: el} = e.data,
				{recovery} = that.classes;

		el.login.$login.toggleClass(recovery);
	}

	toggleLogin(e) {
		const	{that, elem} = e.data,
				{login} = that.classes;

		elem.$el.toggleClass(login);
	}

	toggleCartList(e) {
		const	{that, elem} = e.data,
				{cartListOpen} = that.classes;

		elem.$el.toggleClass(cartListOpen);
	}

	checkMobile(e) {
		let that = {};

		if(e !== undefined && e !== null){
			that = e.data.that;
		}
		else {
			that = this;
		}

		that.isMobile = window.innerWidth < this.desktopWidth;
	}

	// hideTop(e) {
	// 	const {that, elem} = e.data;

	// 	if(!that.isMobile){
	// 		const needHide = window.scrollY > 10;
	// 		const {hideTop} = that.classes;

	// 		elem.$el.toggleClass(hideTop, needHide);
	// 	}
	// }

	mobileMenu(e) {
		const {that, elem} = e.data;
		const {mobileMenu} = that.classes;

		elem.$el.toggleClass(mobileMenu);
	}

	hideShade(e) {
		const {that, elem} = e.data;
		const {mobileMenu, search, login} = that.classes;

		if(elem.$el.hasClass(mobileMenu)) {
			that.mobileMenu({data: e.data});
		}

		elem.$el.toggleClass(login, false);

		if(elem.$el.hasClass(search)) {
			elem.$el.removeClass(search);
			elem.$searchList.fadeOut();
		};
	}

	expandSearch(e) {
		const	{that, elem} = e.data,
				{expandSearch} = that.classes;

		elem.$el.addClass(expandSearch);

		if(e.type != 'focusin') {
			elem.$searchField.trigger('field:focus');
		};

		elem.$searchField.on('field:focusout', {that, elem}, that.turnSearch);
	}

	turnSearch(e) {
		const	{that, elem} = e.data,
				{expandSearch, search} = that.classes;

		elem.$el.removeClass(expandSearch);
		elem.$searchField.off('field:focusOut');
	}

	renderCart(e, {cart_count: count, cart_sum: sum, Items: items}) {
		const	{that, elem: el} = e.data,
				{cartListOpen, amountHide} = that.classes;
		let list = '';

		if (count > 0){
			el.cart.$amount.text(count).removeClass(amountHide);
		}

		for (let el in items){
			let {Name: name, TotalPrice: price} = items[el];
			list += `
			<div class="head__cart-list-el">
				<div class="head__cart-list-el-title">${name}</div>
				<div class="head__cart-list-el-price">${price} ₽</div>
				</div>
			`;
		};

		el.cart.$list.text('').html(list);
		el.cart.$order.text(`Заказать за ${sum} ₽`);

		if(el.data.showList) {
			el.$el.addClass(cartListOpen);
			that.$window.on('click', {that, el}, that.closeCartList);
		}
	}

	closeCartList(e) {
		const 	{that, el} = e.data,
				{cartListOpen} = that.classes,
				clicked = $(e.currentTarget);

		if(!clicked.hasClass(that.selectors.cart.order)) {
			el.$el.removeClass(cartListOpen);
		}

		that.$window.off('click', that.closeCartList);
	}

	searchSet(e, data) {
		const 	{that, elem: el} = e.data,
				{value} = data;

		if(el.data.timer) {
			clearTimeout(el.data.timer);
		}
		if (value != '') {
			el.data.timer = setTimeout(that.sendQuery, 1000, {that, el});
		}
		else {
			that.hideShade({data: {that, elem: el}});
		}
	}

	sendQuery(data) {
		const	{that, el} = data,
				url = el.$searchForm.data('action'),
				callback = that.renderSearch,
				context = {that, el};
		let formData = el.$searchForm.serialize();

		formData += '&' + $.param(el.$searchForm.data());

		that.$window.trigger('ajax:get', {url, data: formData, callback, context});
	}

	renderSearch({data, context}) {
		const 	{that, el} = context,
				{searchAdd, searchBtn} = that.selectors;
		let 	list = '';

		for(let key in data.Items) {
			let el = data.Items[key];
			list += `
			<a href="${el.URL}" class="head__search-list-el">
				<span class="head__search-list-img-wrap"><img class="head__search-list-img" src="${el.Image}" alt="${el.FullName}">
				</span>
				<span class="head__search-list-params">
					<span class="head__search-list-price">${el.ItemPrice} ₽</span>
					<span class="head__search-list-weight">${el.Units}</span>
				</span>
				<span class="head__search-list-subtitle">${el.NamePrefix + ' ' + el.Name}</span>
				<span class="head__search-list-add">
					<span class="head__search-list-title">${el.Name}</span>
					<span class="head__search-list-icon js-head-search-add" data-${el.Field}=${el.Count} data-json="1" data-cart_mode="add">
						<svg class="icon">
							<use xlink:href="#cart"></use>
						</svg>
					</span>
				</span>
			</a>`
		};
		if(data.total > el.data.searchItemCount) {
			list += `<button class="button js-button head__search-list-button js-head-search-btn">Показать все ${data.total}</button>`;
		}
		el.$searchList.html(list);
		el.$el
			.addClass(that.classes.search)
			.find(searchAdd).on('click', {that, el}, that.addToCart);

		el.$searchList.fadeIn();

		el.$el.find(searchBtn).on('click', {that, el}, that.goToCart);
	}

	goToCart(e) {
		e.preventDefault();

		const {el, that} = e.data;

		el.$searchForm.submit();
	}

	addToCart(e) {
		e.preventDefault();

		const 	{that, el} = e.data,
				$target = $(e.currentTarget),
				url = el.data.cartAddUrl,
				data = $target.data(),
				callback = that.addResponce,
				context = {that, el};

		that.$window.trigger('ajax:get', {url, data: $.param(data), callback, context});
	}

	addResponce(data) {
		const	{that, el} = data.context;

		that.$window.trigger(el.data.eventAdd, data.data);
	}

	validateLoginForm(e) {
		e.preventDefault();

		const	{that, el} = e.data,
				$form = $(e.currentTarget).parents('form');
		let valid = true;

		$form.find('.js-field').each((i, item) =>{
			const $el = $(item);
			$el.trigger('field:validate');
			if($el.data('error') === true){
				valid = false;
			}
		});

		$form.find('.js-button').prop('disabled', !valid);
	}

	sendLoginForm(e) {
		e.preventDefault();

		const	{that, elem: el} = e.data,
				$form = $(e.target).parents('form'),
				url = $form.prop('action'),
				data = $form.serialize(),
				callback = that.loginResponce,
				context = {that, el, recovery: $form.hasClass('head__login-recovery')};

		that.$window.trigger('ajax:get', {url, data, callback, context});

	}

	loginResponce(data) {
		const 	{el, that, recovery} = data.context,
				{result, message} = data.data,
				{$loginMsg} = el.login;

		if(recovery) {
			$loginMsg.text(message);
		}

		else {
			if(result) {
				location.reload();
			}
			else {
				$loginMsg.text(message);
			}
		}
	}
}
