class CartPage {
	constructor() {
		this.selectors = {
			el: '.js-cart-page',
			itemsWrap: '.js-items',
			goods: '.js-cart-item',
			offers: '.js-offers',
			offTitle: '.js-offers-title',
			address: '#address',
			city: '.cart-page__form-city .js-switch',
			savedAddr: '.js-field-options',
			cityText: '.js-city-delivery',
			cityDate: '.js-city-date',
			form: '.js-cart-page-form',
			payment: '.js-payment-method',
			deliveryId: '.js-delivery-id',
			deliveryPrice: '.js-delivery-price',
			inMkad: '.js-in-mkad',
			mkadDist: '.js-mkad-dist',
			email: '.js-cart-page-email',
			bill: '.js-cert-page-bill'
		};

		this.data = {
			eventGoodsAdd: 'event-goods-add'
		}

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({itemsWrap, goods, offers, offTitle, address, city, savedAddr, cityText, cityDate, form, payment, deliveryId, deliveryPrice, inMkad, mkadDist, email, bill}, data) {
		const {eventGoodsAdd} = data;

		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$itemsWrap: $el.find(itemsWrap),
				$goods: $el.find(goods),
				$offers: $el.find(offers),
				$offTitle: $el.find(offTitle),
				$address: $el.find(address),
				$city: $el.find(city),
				$savedAddr: $el.find(savedAddr),
				$cityText: $el.find(cityText),
				$cityDate: $el.find(cityDate),
				$form: $el.find(form),
				$payment: $el.find(payment),
				$deliveryId: $el.find(deliveryId),
				$deliveryPrice: $el.find(deliveryPrice),
				$inMkad: $el.find(inMkad),
				$mkadDist: $el.find(mkadDist),
				$email: $el.find(email),
				$bill: $el.find(bill),
				data: {
					eventGoodsAdd: $el.data(eventGoodsAdd) || 'cartItem:add',
					cityChanged: false,
					email: false,
					cityDateName: $el.find(cityDate).not( ":hidden" ).find('.js-select-native').prop('name')
				}
			}
		});
		return this;
	}

	init() {
		this.$window.trigger('cart:dontShowList');

		this.elems.forEach(el => {
			el.$bill.on('click', {that: this}, this.showBill);

			el.$goods
				.on('cartItem:change', {that: this, el}, this.goodsChange)
				.on(el.data.eventGoodsAdd, {that: this, el}, this.goodsAdd);

			el.$city
				.on('switch:change', {that: this, el}, this.setCity)
				.trigger('switch:get');
			el.$savedAddr.on('field-option:change', {that: this, el}, this.reloadSuggestions);

			el.$form.sisyphus();

			this.$window
				.on('order:payment', {that: this, el}, this.changePayment)
				.on('cart:deliveryRecalc', {that: this, el}, this.deliveryRecalc)
				.on('cart:getEmail', {that: this, el}, this.getEmail)
				.on('cart:validate', {that: this, el}, this.validateForm);

		});
	}

	showBill(e) {
		const 	{that} = e.data;

		that.$window.trigger('bill:show');
	}

	getEmail(e) {
		const	{that, el} = e.data;
		let email = false;

		el.$email.trigger('field:validate');
		if(el.$email.data('error') !== 'true') {
			email = el.$email.find('.js-field-input').val();
		}

		that.$window.trigger('cart:email', {email});
	}

	validateForm(e) {
		const	{that, el} = e.data;
		let valid = true;

		el.$form.find('.js-field').each((i, item) =>{
			const $el = $(item);
			$el.trigger('field:validate');
			if($el.data('error') === true){
				valid = false;
			}
		});

		if(el.data.deliveryIsValid === undefined) {
			el.$savedAddr.trigger('field-option:validate');
		}

		if(valid && el.data.deliveryIsValid){
			that.$window.trigger('cart:submit');
			// el.$form.serialize();
			el.$form.submit();
		}
		else {
			console.log('Form is not valid.');
		}
	}

	deliveryRecalc(e) {
		const	{that, el} = e.data;

		if(el.$address.val() !== ''){
			that.getDeliveryPrice(el);
		}
	}

	changePayment(e, data) {
		const	{that, el} = e.data,
				{value} = data;

		el.$payment.val(value);
	}

	setCity(e, data) {
		const	{that, el} = e.data,
				{kladr, id} = data.data;

		that.showCityData({that, el, id});

		let constraints = kladr.split(',').map((val) => {
			return {
				locations: {kladr_id: val.replace(' ', '')}
			}
		});

		if(!el.data.cityChanged) {
			el.$savedAddr.find('.js-field').trigger('field:empty');
		}
		else {
			el.data.cityChanged = false;
		}

		el.data.suggestions = el.$address.suggestions({
			token: "670831253184a6570dcfb29298d37c4800de6d75",
			type: "ADDRESS",
			constraints: constraints,
			onSelect: (suggestion) => {
				console.log(suggestion);
				that.getDeliveryPrice(el);
			}
		}).suggestions();
	}

	showCityData({that, el, id}) {
		el.$cityText.each((i, elem) => {
			const $el = $(elem);
			if(i == id) {
				$el.show();
			}
			else {
				$el.hide();
			}
		});
		el.$cityDate.each((i, elem) => {
			const $el = $(elem);
			if(i == id) {
				$el.show().find('.js-select-native').prop('name', el.data.cityDateName);
			}
			else {
				$el.hide().find('.js-select-native').prop('name', '');
			}
		});
	}

	reloadSuggestions(e, {data}) {
		const	{el, that} = e.data;
		let id;

		if(!data.verified){
			el.data.suggestions.update();
		}
		else {
			if(data.city == 'spb') {
				id = 1;
			}
			else if(data.city == 'moscow') {
				id = 0;
			}
			el.data.cityChanged = true;
			el.$city.trigger('switch:update', {id});
			that.getDeliveryPrice(el);
		}
	}

	getDeliveryPrice(el) {
		const	data = el.$form.serialize(),
				url = el.$form.data('url-delivery'),
				ajaxMethod = el.$form.data('ajax-method') || 'POST',
				callback = this.deliveryPrice;

		this.$window.trigger('ajax:get', {url, type: ajaxMethod, data, callback, context: {that: this, el}});
	}

	deliveryPrice({data, context}) {
		const	{inMKAD, MkadDistance, price, id, error} = data,
				{that, el} = context,
				{$deliveryId, $deliveryPrice, $inMkad, $mkadDist, $address} = el;

		if(error === undefined) {
			$deliveryId.val(id);
			$deliveryPrice.val(price);
			$inMkad.val(inMKAD);
			$mkadDist.val(MkadDistance);

			el.data.deliveryIsValid = true;

			$address.trigger('field:error', {show: false});

			that.$window.trigger('cart:delivery', data);
		}
		else {
			el.data.deliveryIsValid = false;
			$address.trigger('field:error', {show: true, text: error});
		}
	}

	goodsAdd(e) {
		const	{el, that} = e.data,
				item = $(e.target).parent();

		el.$itemsWrap.append(item);

		if(el.$offers.find('div').length == 0) {
			el.$offTitle.hide();
		}
	}

	goodsChange(e, data) {
		const	{that, el} = e.data;

		console.log(data);
	}
}
