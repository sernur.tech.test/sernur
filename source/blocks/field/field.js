class Field {
	constructor() {
		this.selectors = {
			el: '.js-field',
			label: '.js-field-label',
			input: '.js-field-input',
			clear: '.js-field-clear',
			extra: '.js-field-extra',
			wrap: '.js-field-wrap',
			error: '.js-field-error'
		};

		this.data = {
			mask: 'mask',
			clearEvent: 'clear-event',
			eventFocus: 'event-focus',
			eventError: 'event-error',
			eventFocusOut: 'event-focus-out'
		};

		this.classes = {
			focus: 'field_focus',
			error: 'field_error',
			extra: 'field_extra',
			checkbox: 'field_checkbox',
			checked: 'field_checked'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get()
				.init();
		}
	}

	get() {
		this.elems = this.el.toArray().map(this.addEl, this);
		return this;
	}

	addEl(el){
		const	{input, clear, extra, wrap, error, label} = this.selectors,
				{clearEvent, eventFocus, eventError, mask, eventFocusOut} = this.data,
				$el = $(el);

		return {
			$el,
			$input: $el.find(input),
			$label: $el.find(label),
			$clear: $el.find(clear),
			$extra: $el.find(extra),
			$wrap: $el.find(wrap),
			$error: $el.find(error),
			data: {
				mask: $el.data(mask),
				clear: $el.data(clearEvent) || 'field:clear',
				focus: $el.data(eventFocus) || 'field:focus',
				error: $el.data(eventError) || 'field:error',
				focusOut: $el.data(eventFocusOut) || 'field:focusout',
				checkbox: $el.hasClass(this.classes.checkbox)
			}
		}
	}

	init() {
		this.elems.forEach(this.initEl, this);
		this.$window.on('field:add', {that: this}, this.newEl)
	}

	initEl(elem) {
		elem.$input
			.on('focusin focusout', {elem, that: this}, this.focus)
			.on('change', {elem, that: this}, this.change)
			.on('input', {elem}, this.input);

		elem.$el
			.on('field:update', {elem, that: this}, this.updateValue)
			.on('field:extra', {elem, that: this}, this.extra)
			.on('field:empty', {elem}, this.empty)
			.on('field:validate', {elem, that: this}, this.runValidate)
			.on('field:disable', {elem, that: this}, this.setDisabled)
			.on(elem.data.focus, {elem}, this.setFocus)
			.on(elem.data.error, {elem, that: this}, this.showError)
			.on('field:copy', {elem, that: this}, this.copyAction);

		if(elem.data.checkbox) {
			elem.$wrap.on('click', {elem, that: this}, this.checkboxChange);
		}

		if(elem.data.mask) {
			elem.numbered = new Numbered(elem.$input, {
				mask: elem.data.mask,
				empty: '_'
			});
		}

		elem.$clear.on('click', {elem}, this.clear);
	}

	copyAction(e) {
		const {elem, that} = e.data;

		elem.$input[0].select();
		document.execCommand('copy');
		elem.$el.trigger('field:copied');
	}

	setDisabled(e, {disabled}) {
		const 	{elem, that} = e.data;

		elem.$input.prop('readonly', disabled);
	}

	newEl(e, data) {
		const 	{that} = e.data,
				{el} = data;

		const newEl = that.addEl(el[0]);
		that.elems.push(newEl);
		that.initEl(newEl);
	}

	showError(e, data) {
		const 	{elem, that} = e.data,
				{extra, error} = that.classes,
				{show, text} = data;

		elem.$el.removeClass(extra);

		if(text !== undefined) {
			elem.$error.text(text);
		}
		elem.$el.toggleClass(error, show);
	}

	extra(e, data = false) {
		const 	{that, elem} = e.data,
				{text} = data,
				{extra, error} = that.classes,
				show = data.show !== false;

		elem.$el.removeClass(error);

		if(text !== undefined){
			elem.$extra.text(text);
		}
		elem.$el.toggleClass(extra, show);
	}

	setFocus(e) {
		const {elem} = e.data;

		elem.$input.focus();
	}

	clear(e) {
		const	{elem} = e.data,
				{$input, $label} = elem,
				value = $input.val(),
				label = $label ? $label.text() : false;

		elem.$el.trigger(elem.data.clear, {value, label});
	}

	empty(e) {
		const {$input} = e.data.elem;

		$input.val('');
	}

	checkboxChange(e) {
		const	{that, elem} = e.data,
				{checked} = that.classes,
				isChecked = elem.$el.hasClass(checked);

		elem.$input
			.prop('checked', !isChecked)
			.trigger('change');

	}

	change(e) {
		const	{that, elem} = e.data,
				{$el, $input, numbered} = e.data.elem,
				type = $el.data().validate,
				{checked} = that.classes,
				{$label} = elem;
		let	value = $input.val();
		const label = $label ? $label.text() : false;

		$el.trigger('field:change', {value, label});

		if(elem.data.checkbox) {
			$el.toggleClass(checked, $input.prop('checked'));
			value = $input.prop('checked');
		}


		if(type) {
			that.validate({that, $el, type, value, numbered});
		}
	}

	input(e) {
		const	{elem} = e.data,
				value = elem.$input.val();

		elem.$el.trigger('field:input', {value});
	}

	runValidate(e) {
		const	{that, elem} = e.data,
				{$el, $input, numbered} = elem,
				checked = $input.prop('checked'),
				type = $el.data().validate;
		let	value = $input.val();

		if(elem.data.checkbox) {
			value = $input.prop('checked');
		}

		if(type) {
			that.validate({that, $el, type, value, numbered});
		}
	}

	validate({that, $el, type, value, numbered}) {
		const text = /^(\w|\d|[А-я,Ё,ё]){2,}/;
		const notEmpty = /.{2,}/;
		const email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		let hasError = false;

		if(value !== '') {
			switch (type) {
				case 'text':
					hasError = value.match(text) === null;
					break;
				case 'notEmpty':
					hasError = value.match(notEmpty) === null;
					break;
				case 'email':
					hasError = value.toLowerCase().match(email) === null;
					break;
				case 'tel':
					hasError = value.replace(/[^\d]/gi, '').length < 10;
					break;
				case 'checkbox':
					hasError = !value;
					break;
				case 'mask':
					hasError = numbered.validate() < 0;
					break;
			}
		}
		else {
			hasError = true;
		}

		$el.data('error', hasError);
		$el.toggleClass(that.classes.error, hasError);
	}

	updateValue(e, data) {
		const 	{elem, that} = e.data,
				{value} = data;

		elem.$input.val(value);
	}

	focus(e) {
		const {elem, that} = e.data;
		elem.$el.toggleClass(that.classes.focus, e.type == 'focusin');
		if(e.type == 'focusout') {
			elem.$el.trigger(elem.data.focusOut);
		}
	}
}